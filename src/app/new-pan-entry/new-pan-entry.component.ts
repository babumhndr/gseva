import { Component, OnInit, ViewChild } from '@angular/core';
import {HttpModule, Http, URLSearchParams, Headers, RequestOptions} from '@angular/http';
import{ Router, ActivatedRoute, Params } from '@angular/router';
import {MatMenuModule} from '@angular/material/menu';
import {FormControl, Validators, ReactiveFormsModule, FormsModule} from '@angular/forms';
import {FormGroupDirective, NgForm} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { GsevaService } from '../gseva.service';
import { LoadingModule } from 'ngx-loading';


@Component({
  selector: 'app-new-pan-entry',
  templateUrl: './new-pan-entry.component.html',
  styleUrls: ['./new-pan-entry.component.css'],
  providers:[GsevaService]
})
export class NewPanEntryComponent implements OnInit {

  constructor(private ref:GsevaService, private http:Http, private router:Router) { }

  @ViewChild(NgForm) myForm: NgForm;

  public loading:boolean = false;
  apiRoot: string = this.ref.baseURL();
  msg:string;
  error:boolean=false;
  isAdmin:boolean=false;

  public formData:FormData;
  file3:File;
  submitBtn:boolean=true;
  fileName:string="dummy.pdf";
  sameAddr:any;
  dispatched_address:any;
  permanent_address:any;
  aadharDiv:boolean=false;
  EIDDIV:boolean=false;
  panNumberDiv:boolean=false;
  type:any
  photo:string="";
  ngOnInit() {
  	if (localStorage.getItem('userType')=='admin') {
  		this.isAdmin=true;
  	}
  }

  copyAddress(e){
  	if(e.target.checked){
  		this.dispatched_address=this.permanent_address;
  	}else{
  		this.dispatched_address='';	
  	}
  }

  set_aadhar_eid(val){
  	if(val=='EID'){
  			this.EIDDIV=true;
  			this.aadharDiv=false;
  	}else{
  			this.aadharDiv=true;
  			this.EIDDIV=false;
  	}
  }

  fileUpload(event){
  	this.submitBtn=false;
  	this.formData=  new FormData();
  	this.file3 = event.target.files[0];
  	var fileExtension = '.' + this.file3.name.split('.').pop();
  	if (fileExtension=='.pdf') {
  		var name=localStorage.getItem('username')+"_"+localStorage.getItem('id')+"_"+Date.now()+fileExtension;
  		this.fileName=name;
		  /*	var fileExtension = '.' + this.file3.name.split('.').pop();

		 	 this.file3.name = Math.random().toString(36).substring(7) + new Date().getTime() + fileExtension;
		*/
		    
		    console.log(this.file3);
		  	this.formData.append(event.target.name, this.file3, name);
		  	console.log(this.formData);
		  	 let url =this.ref.baseURL()+'pan/upload.php';
		 
			this.http.post(url,this.formData)
                  .subscribe((result) => {
                    console.log(result);
                    alert(result._body);
                    this.submitBtn=true;
                    
                  });
  	}else{
  		alert("Please choose .pdf file only");
  	}
  }

  photoUpload(event){
  	this.submitBtn=false;
  	this.formData=  new FormData();
  	this.file3 = event.target.files[0];
  	var fileExtension = '.' + this.file3.name.split('.').pop();
  	if (fileExtension=='.jpg' || fileExtension=='.png' || fileExtension=='.JPG') {
  		var name="gseva"+localStorage.getItem('username')+"_"+localStorage.getItem('id')+"_"+Date.now()+fileExtension;
  		this.photo=name;
		  /*	var fileExtension = '.' + this.file3.name.split('.').pop();

		 	 this.file3.name = Math.random().toString(36).substring(7) + new Date().getTime() + fileExtension;
		*/
		    
		    console.log(this.file3);
		  	this.formData.append(event.target.name, this.file3, name);
		  	console.log(this.formData);
		  	 let url =this.ref.baseURL()+'pan/uploadPhoto.php';
		 
			this.http.post(url,this.formData)
                  .subscribe((result) => {
                    console.log(result);
                    alert(result._body);
                    this.submitBtn=true;
                    
                  });
  	}else{
  		alert("Please choose .pdf file only "+fileExtension);
  	}
  }

  ifCorrection(event: any){
  	console.log(this.type);
  	if (this.type=='Correction') {
  		this.panNumberDiv=true;
  	}else{
  		this.panNumberDiv=false;
  	}
  }

  newPan(value){
  	console.log(value);
  	if (value.Aadhaar_EID==undefined) {value.Aadhaar_EID='';} 
  	if (value.Category_of_Applicant==undefined) {value.Category_of_Applicant='';} 
  	if (value.DOB==undefined) {value.DOB='';} 
  	if (value.POA==undefined) {value.POA='';} 
  	if (value.POB==undefined) {value.POB='';} 
  	if (value.POI==undefined) {value.POI='';} 
  	if (value.addressForCommunication==undefined) {value.addressForCommunication='';} 
  	if (value.date==undefined) {value.date='';} 
  	if (value.dispatched_address==undefined) {value.dispatched_address='';} 
  	if (value.email==undefined) {value.email='';} 
  	if (value.fatherFirstName==undefined) {value.fatherFirstName='';} 
  	if (value.fatherLasttName==undefined) {value.fatherLastName='';} 
  	if (value.fatherMiddleName==undefined) {value.fatherMiddleName='';} 
  	if (value.fees==undefined) {value.fees='';} 
  	if (value.firstName==undefined) {value.firstName='';} 
  	if (value.lastName==undefined) {value.lastName='';} 
  	if (value.middleName==undefined) {value.middleName='';} 
  	if (value.nameAsPerAadhar==undefined) {value.nameAsPerAadhar='';} 
  	if (value.nameOnCard==undefined) {value.nameOnCard='';} 
  	if (value.panCardDispatchedState==undefined) {value.panCardDispatchedState='';} 
  	if (value.stdCode==undefined) {value.stdCode='';} 
  	if (value.telephoneNumber==undefined) {value.telephoneNumber='';} 
  	if (value.type==undefined) {value.type='';} 
  	if (value.whatsAppNumber==undefined) {value.whatsAppNumber='';} 
  	if (value.RAaddressx==undefined) {value.RAaddressx='';} 
  	if (value.permanent_address==undefined) {value.permanent_address='';} 
  	if (value.Aadhar_Number==undefined) {value.Aadhar_Number='';} 
  	if (value.EID==undefined) {value.EID='';} 
  	if (value.panNumber==undefined) {value.panNumber='';} 
  	if (value.remarks==undefined) {value.remarks='';} 

  	if (value.EIDdate==undefined) {value.EIDdate='';} 
  	if (value.panNumber==undefined) {value.panNumber='';} 
  
  	if (value.remarks==undefined) {value.remarks='';} 

  	value.file=this.fileName;
  	value.file=this.photo;
  	value.user_id=localStorage.getItem('id');
  	value.tracking_id=localStorage.getItem('id')+Date.now();
  	value.date=Date.now();
  	
  	console.log('afetr validation',value);

    let headers: Headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let opts = new RequestOptions();
    opts.headers = headers;
    this.http.post(this.ref.baseURL()+'pan/new_pan_entry.php',value,opts)
                  .map(response => response.json())
                  .subscribe((result) => {
                    console.log(result);
                    if(result.status=="failure"){
                      this.error=true;
                      this.msg=result.message;
                      this.loading = false;
                      alert(result.message);
                    }else if(result.status=="success"){
                      alert(result.message);
                      this.myForm.resetForm();
                    }else{
                    	alert("Please enter all the required feilds.");
                    }
                  });

  }


  processingFees=  [
    {value: '110'},
    {value: '150'},
    {value: '170'},
    {value: '200'}
  ];

  types = [
    {value: 'New'},
    {value: 'Correction'}
  ];

  category = [
    {value: 'Individual', viewValue: 'Individual'},
    {value: 'Firm', viewValue: 'Firm'},
    {value: 'Body of Individuals', viewValue: 'body of Individuals'},
    {value: 'Hindu of Undivided Family', viewValue: 'Hindu of Undivided Family'},
    {value: 'Association of Persons', viewValue: 'Association of Persons'},
    {value: 'Local Authority', viewValue: 'Local Authority'},
    {value: 'Company', viewValue: 'Company'},
    {value: 'Trust', viewValue: 'Trust'},
    {value: 'Artificial Juridicial Person', viewValue: 'Artificial Juridicial Person'},
    {value: 'Government', viewValue: 'Government'},
    {value: 'Limited liability Partnership', viewValue: 'Limited liability Partnership'}
  ];

  address = [
    {value: 'Indian', viewValue: 'Indian'},
    {value: 'Foreign', viewValue: 'Foreign'}
  ];
  RAaddress = [
    {value: 'Indian', viewValue: 'Indian'},
    {value: 'Foreign', viewValue: 'Foreign'}
  ];

  POIs = [
    {value: 'Certificate of identity signed by a Gazetted Officer', viewValue: 'Certificate of identity signed by a Gazetted Officer'},
    {value: 'Certificate of identity signed by a Member of Legisiative assembly', viewValue: 'Certificate of identity signed by a Member of Legisiative assembly'},
    {value: 'Certificate of identity signed by a member Of Parliament', viewValue: 'Certificate of identity signed by a member Of Parliament'},
    {value: 'Certificate of identity signed by a Municiple Conuncillor', viewValue: 'Certificate of identity signed by a Municiple Conuncillor'},
    {value: 'Driving License', viewValue: 'Driving License'},
    {value: 'Passport', viewValue: 'Passport'},
    {value: 'Arm\'s License', viewValue: 'Arm\'s License'},
    {value: 'Central Government Health Scheme Card', viewValue: 'Central Government Health Scheme Card'},
    {value: 'Ex-Servicemen Conrtibutory Health Scheme Photo Card', viewValue: 'Ex-Servicemen Conrtibutory Health Scheme Photo Card'},
    {value: 'Bank Certificate In Orginal On letter head from the branch (along with name and stamp of the issuing officer)Contain', viewValue: 'Bank Certificate In Orginal On letter head from the branch (along with name and stamp of the issuing officer)Contain'},
    {value: 'Photo identity Card issued by the central Government or State Government or Public Sector undertaking', viewValue: 'Photo identity Card issued by the central Government or State Government or Public Sector undertaking'},
    {value: 'Pensioner Card having photograph of the applicantElector\'s Photo Identity Card', viewValue: 'Pensioner Card having photograph of the applicantElector\'s Photo Identity Card'},
    {value: 'Elector\'s Photo Identity Card', viewValue: 'Elector\'s Photo Identity Card'},
    {value: 'Ration card having photograph of the applicant', viewValue: 'Ration card having photograph of the applicant'},
    {value: 'AADHAAR Card Issued by the Unique Identification Authority of India', viewValue: 'AADHAAR Card Issued by the Unique Identification Authority of India'}
  ];

  POAs = [
  	{value: 'Latest Property tax assessment order'},
  	{value: 'AADHAAR Card Issued by the Unique Identification Authority of India'},
	{value: 'Depository account atatement (Not more than 3 months old from the date of application)'},
	{value: 'Credit Card statement (Not More than 3 months old from the date of application)'},
	{value: 'Bank account statement/passbook (Not More than 3 months old from the date of application) '},
	{value: 'landline telephone Bill (Not More than 3 months old from the date of application)'},
	{value: 'Certificate of address signed by a Municiple Councillor'},
	{value: 'Driving licence'},
	{value: 'Passport'},
	{value: 'Property Registation Document'},
	{value: 'Electricity Bill (Not More than 3 months old from the date of application) '},
	{value: 'Bank account statement in the country of residence (Not More than 3 months old from the date of application) '},
	{value: 'NRE bank Account statement (Not More than 3 months old from the date of application) '},
	{value: 'Employer certificate in orginal '},
	{value: 'Elector\'s Photo Identity Card'},
	{value: 'Certificate of identity signed by a Gazetted Officer'},
	{value: 'Passport of the Spouse'},
	{value: 'Post Office passbook having address of the applicant'},
	{value: 'Domicile certificate issued by the Government'},
	{value: 'Allotment letter of accommodation issued by central or State Government of not more than three years old'},
	{value: 'Certificate of address signed by a Member of legislative Assembly'},
	{value: 'Consumer Gas Connection card or book or piped gas bill(Not more than 3 months old from date of application)'},
	{value: 'Water Bill(Not more than 3 months old from the date of application)'},
	{value: 'Broadband Connection Bill (Not more than 3 months old from date of application)'},
	{value: 'Employee certificate in orginal'}
  ];

  POBs=[
  	{value:'Birth certifiacte issued by the Municiple Authority or any office authorized to issue Birth And Death Certificate by the Pension Payment order'},
	{value:'Marriage certificate issued by Register Of marriages'},
	{value:'Matriculation certificate'},
	{value:'Passport'},
	{value:'Driving License'},
	{value:'Domicile certificate issued by the Government'},
	{value:'Affidavit sworn before a magistrate stating the date of birth'},
	{value:'Matriculation Markssheet of recognised board'},
	{value:'AADHAAR Card Issued by the Unique Identification Authority of India'},
	{value:'Elector\'s Photo Identity Card'},
	{value:'Photo identity Card issued by the central Government or State Government or Public Sector undertaking'},
	{value:'Central Government Health Scheme Photo card'},
	{value:'Ex-Servicemen Conrtibutory Health Scheme Photo Card'},
  ];

  aadharEID = [
    {value: 'Aadhaar No', viewValue: 'Aadhaar No'},
    {value: 'EID', viewValue: 'EID'}
  ];

  states = [
			{
			"code": "AN",
			"name": "Andaman and Nicobar Islands"
			},
			{
			"code": "AP",
			"name": "Andhra Pradesh"
			},
			{
			"code": "AR",
			"name": "Arunachal Pradesh"
			},
			{
			"code": "AS",
			"name": "Assam"
			},
			{
			"code": "BR",
			"name": "Bihar"
			},
			{
			"code": "CG",
			"name": "Chandigarh"
			},
			{
			"code": "CH",
			"name": "Chhattisgarh"
			},
			{
			"code": "DH",
			"name": "Dadra and Nagar Haveli"
			},
			{
			"code": "DD",
			"name": "Daman and Diu"
			},
			{
			"code": "DL",
			"name": "Delhi"
			},
			{
			"code": "GA",
			"name": "Goa"
			},
			{
			"code": "GJ",
			"name": "Gujarat"
			},
			{
			"code": "HR",
			"name": "Haryana"
			},
			{
			"code": "HP",
			"name": "Himachal Pradesh"
			},
			{
			"code": "JK",
			"name": "Jammu and Kashmir"
			},
			{
			"code": "JH",
			"name": "Jharkhand"
			},
			{
			"code": "KA",
			"name": "Karnataka"
			},
			{
			"code": "KL",
			"name": "Kerala"
			},
			{
			"code": "LD",
			"name": "Lakshadweep"
			},
			{
			"code": "MP",
			"name": "Madhya Pradesh"
			},
			{
			"code": "MH",
			"name": "Maharashtra"
			},
			{
			"code": "MN",
			"name": "Manipur"
			},
			{
			"code": "ML",
			"name": "Meghalaya"
			},
			{
			"code": "MZ",
			"name": "Mizoram"
			},
			{
			"code": "NL",
			"name": "Nagaland"
			},
			{
			"code": "OR",
			"name": "Odisha"
			},
			{
			"code": "PY",
			"name": "Puducherry"
			},
			{
			"code": "PB",
			"name": "Punjab"
			},
			{
			"code": "RJ",
			"name": "Rajasthan"
			},
			{
			"code": "SK",
			"name": "Sikkim"
			},
			{
			"code": "TN",
			"name": "Tamil Nadu"
			},
			{
			"code": "TS",
			"name": "Telangana"
			},
			{
			"code": "TR",
			"name": "Tripura"
			},
			{
			"code": "UP",
			"name": "Uttar Pradesh"
			},
			{
			"code": "UK",
			"name": "Uttarakhand"
			},
			{
			"code": "WB",
			"name": "West Bengal"
			}
		];
}
