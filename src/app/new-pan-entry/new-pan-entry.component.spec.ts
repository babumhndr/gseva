import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewPanEntryComponent } from './new-pan-entry.component';

describe('NewPanEntryComponent', () => {
  let component: NewPanEntryComponent;
  let fixture: ComponentFixture<NewPanEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewPanEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPanEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
