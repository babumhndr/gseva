import { Component, OnInit, ViewChild } from '@angular/core';
import {HttpModule, Http, URLSearchParams, Headers, RequestOptions} from '@angular/http';
import{ Router, ActivatedRoute, Params } from '@angular/router';
import {MatMenuModule} from '@angular/material/menu';
import {FormControl, Validators, ReactiveFormsModule, FormsModule} from '@angular/forms';
import {FormGroupDirective, NgForm} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { GsevaService } from '../gseva.service';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {MatTableModule} from '@angular/material/table';
import { LoadingModule } from 'ngx-loading';

@Component({
  selector: 'app-pan-list',
  templateUrl: './pan-list.component.html',
  styleUrls: ['./pan-list.component.css'],
  providers:[GsevaService]
})
export class PanListComponent implements OnInit {

 
displayedColumns = ['id', 'name', 'progress', 'color'];
  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private ref:GsevaService, private http:Http, private router:Router) {
    // Create 100 users
   

    
  }

  public loading:boolean = false;
  apiRoot: string = this.ref.baseURL();
  msg:string;
  error:boolean=false;
   panList: UserData[] ;
  actionsDiv:boolean=true;
  panId:any;
  isAdmin:boolean=false;

  ngOnInit() {
    let userType=localStorage.getItem('userType');
    if (userType=='admin') {
      this.isAdmin=true
    }
    this.getPanList();
  }

  showActions(value){
    this.panId=value;
  }

  hideActions(){
    this.actionsDiv=false;

  }
  /**
   * Set the paginator and sort after the view init since this component will
   * be able to query its view for the initialized paginator and sort.
   */
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
 getPanList(){
       this.panList=[];
      let id=localStorage.getItem('id');
      let headers: Headers = new Headers();
      headers.append('Content-Type', 'application/json');
      let opts = new RequestOptions();
      opts.headers = headers;
      let url:string = this.ref.baseURL()+'pan/pan_list.php';
      this.http.post(url,id,opts).map(response => response.json()).subscribe((res)=>{
        console.log(res);  
        // Assign the data to the data source for the table to render
        
        this.panList=res.data;
     
          });
  }
  updatePan(value){
    if (value.pan_number==undefined) {value.pan_number='';}
    if (value.acknowledgementNumber==undefined) {value.acknowledgementNumber='';}
    if (value.courier_tracking_number==undefined) {value.courier_tracking_number='';}
    if (value.status==undefined) {value.status='';}
    value.id=this.panId;

    console.log(value);

    let headers: Headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let opts = new RequestOptions();
    opts.headers = headers;
    this.http.post(this.ref.baseURL()+'pan/update_pan.php',value,opts)
                  .map(response => response.json())
                  .subscribe((result) => {
                    console.log(result);
                    if(result.status=="failure"){
                      this.error=true;
                      this.msg=result.message;
                      this.loading = false;
                      alert(result.message);
                    }else if(result.status=="success"){
                      alert(result.message);
                      location.reload();
                    }else{
                      alert("Something went wrong.");
                    }
                  });
  }

  statuses=  [
    {value: 'By Post Despatched Cards'},
    {value: 'NSDL Despatched Card'},
    {value: 'Recived the Cards Enrty (Only Courier)'},
    {value: 'Ready to Despatch'},
    {value: 'Successfully Despatched'}
  ];



  settings = {
    columns: {
      id: {
        title: 'ID'
      },
      name: {
        title: 'Full Name'
      },
      username: {
        title: 'User Name'
      },
      email: {
        title: 'Email'
      }
    }
  };

}


export interface UserData {
  id: string;
  Category_of_Applicant:string;
  date:string;
  lastName:string;
  middleName:string;
  nameOnCard:string;
  fatherLastName:string;
  fatherFirstName:string;
  fatherMiddleName:string;
  DOB:string;
  stdCode:string;
  telephoneNumber:string;
  whatsAppNumber:string;
  email:string;
  addressForCommunication:string;
  nameAsPerAadhar:string;
  panCardDispatchedState:string;

}