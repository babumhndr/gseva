import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { SelecttimeComponent } from './selecttime/selecttime.component';
import { SeatsComponent } from './seats/seats.component';
import { AgentCreationComponent } from './agent-creation/agent-creation.component';
import { NewPanEntryComponent } from './new-pan-entry/new-pan-entry.component';
import { DomicileComponent } from './domicile/domicile.component';
import { AnnexureComponent } from './annexure/annexure.component';
import { LoginComponent } from './login/login.component';
import {AuthGuard} from './auth.guard';
import { PanListComponent } from './pan-list/pan-list.component';
import { UpdatePanComponent } from './update-pan/update-pan.component';

const routes: Routes = [
	{ path: '',component: NewPanEntryComponent , canActivate : [AuthGuard]},
	{ path: 'seats',component: SeatsComponent, canActivate : [AuthGuard] },
	{ path: 'create-agent',component: AgentCreationComponent , canActivate : [AuthGuard]},
	{ path: 'new-pan-entry',component: NewPanEntryComponent , canActivate : [AuthGuard]},
	{ path: 'domicile',component: DomicileComponent , canActivate : [AuthGuard]},
	{ path: 'annexure',component: AnnexureComponent , canActivate : [AuthGuard]},
	{ path: 'pan-list',component: PanListComponent , canActivate : [AuthGuard]},
	{ path: 'update-pan/:id',component: UpdatePanComponent , canActivate : [AuthGuard]},
	{ path: 'login',component: LoginComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
