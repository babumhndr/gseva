import { Component, OnInit } from '@angular/core';
import {HttpModule, Http, URLSearchParams, Headers, RequestOptions} from '@angular/http';
import{ Router, ActivatedRoute, Params } from '@angular/router';
import { GsevaService } from '../gseva.service';
import { LoadingModule } from 'ngx-loading';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers:[GsevaService]
})
export class LoginComponent implements OnInit {

  constructor(private ref:GsevaService, private http:Http, private router:Router) { }
  public loading:boolean = false;
  apiRoot: string = this.ref.baseURL();
  
  color = 'accent';
  show = false;
  ngOnInit() {
  }

  msg:string;
  error:boolean=false;
  loginSubmit(value){
  	console.log(value);
    this.loading = true;
  	console.log(value);
  	let request = this.http.post(this.ref.baseURL()+'/users/login.php',
               {
			          username:value.username, 
			          password:value.password
			      }
               )
                  .map(response => response.json())
                  .subscribe((result) => {
                  	console.log(result);
                    if(result.status=="failure"){
                      this.error=true;
                      this.msg=result.message;
                      this.loading = false;
                    }else if(result.status=="success"){
                      localStorage.setItem('username', result.data["0"].username);
                      localStorage.setItem('active', result.data["0"].active);
                      localStorage.setItem('userType', result.data["0"].userType);
                      localStorage.setItem('id', result.data["0"].uid);
                      this.router.navigate(['new-pan-entry']);
                    }
                  });
                 
  }


}
