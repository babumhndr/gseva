import { TestBed, inject } from '@angular/core/testing';

import { BdqserviceService } from './bdqservice.service';

describe('BdqserviceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BdqserviceService]
    });
  });

  it('should be created', inject([BdqserviceService], (service: BdqserviceService) => {
    expect(service).toBeTruthy();
  }));
});
