import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule, Http, URLSearchParams, Headers, RequestOptions } from '@angular/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import {FormControl, Validators, ReactiveFormsModule, FormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { HomeComponent } from './home/home.component'
import { NgxCarouselModule } from 'ngx-carousel';
import { SelecttimeComponent } from './selecttime/selecttime.component';
import { FooterComponent } from './footer/footer.component';
import { SeatsComponent } from './seats/seats.component';
import { AgentCreationComponent } from './agent-creation/agent-creation.component';
import { NewPanEntryComponent } from './new-pan-entry/new-pan-entry.component';
import { DomicileComponent } from './domicile/domicile.component';
import { GeneralMenuComponent } from './general-menu/general-menu.component';
import { AnnexureComponent } from './annexure/annexure.component';
import { LoginComponent } from './login/login.component';
import {AuthGuard} from './auth.guard';
import { PanListComponent } from './pan-list/pan-list.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { UpdatePanComponent } from './update-pan/update-pan.component';
import { MatDatepickerModule, MatNativeDateModule, DateAdapter } from '@angular/material';   
import { DateFormat } from './date-format';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SelecttimeComponent,
    FooterComponent,
    SeatsComponent,
    AgentCreationComponent,
    NewPanEntryComponent,
    DomicileComponent,
    GeneralMenuComponent,
    AnnexureComponent,
    LoginComponent,
    PanListComponent,
    UpdatePanComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    MaterialModule,
    FormsModule,
    NgxCarouselModule,
    NgbModule, 
    ReactiveFormsModule,
    HttpModule,
    Ng2SmartTableModule,
    MatDatepickerModule, MatNativeDateModule
  ],
  providers: [{ provide: DateAdapter, useClass: DateFormat },AuthGuard],
  bootstrap: [AppComponent]
})

export class AppModule {
    constructor(private dateAdapter:DateAdapter<Date>) {
        dateAdapter.setLocale('nl'); // DD/MM/YYYY
    }
}