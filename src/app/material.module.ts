import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import { MatSortModule, MatDatepickerModule, MatNativeDateModule, MatProgressBarModule,  MatCheckboxModule,  MatMenuModule , MatIconModule, MatTooltipModule, MatTabsModule, MatSelectModule, MatButtonModule, MatToolbarModule, MatInputModule, MatProgressSpinnerModule, MatCardModule, MatRadioModule, MatSliderModule } from '@angular/material';

@NgModule({
  imports: [MatTableModule, MatPaginatorModule, MatSortModule, MatDatepickerModule, MatNativeDateModule, MatProgressBarModule,  MatCheckboxModule, MatMenuModule, MatIconModule, MatTooltipModule, MatTabsModule, MatSelectModule, MatButtonModule, MatToolbarModule, MatInputModule, MatProgressSpinnerModule, MatCardModule, MatRadioModule, MatSliderModule],
  exports: [MatTableModule,  MatPaginatorModule, MatSortModule, MatDatepickerModule, MatNativeDateModule, MatProgressBarModule, MatCheckboxModule, MatMenuModule,  MatIconModule, MatTooltipModule, MatTabsModule, MatSelectModule, MatButtonModule, MatToolbarModule, MatInputModule, MatProgressSpinnerModule, MatCardModule, MatRadioModule, MatSliderModule],
})
export class MaterialModule { }