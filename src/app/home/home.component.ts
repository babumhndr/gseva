import { Component, OnInit } from '@angular/core';
import {MatMenuModule} from '@angular/material/menu';
import {FormControl, Validators, ReactiveFormsModule, FormsModule} from '@angular/forms';
import {FormGroupDirective, NgForm} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';



/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  emailFormControl = new FormControl('', [ Validators.required,Validators.email,]);

  matcher = new MyErrorStateMatcher();
}
