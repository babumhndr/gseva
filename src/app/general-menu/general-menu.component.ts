import { Component, OnInit } from '@angular/core';
import {HttpModule, Http, URLSearchParams, Headers, RequestOptions} from '@angular/http';
import{ Router, ActivatedRoute, Params } from '@angular/router';
import {MatMenuModule} from '@angular/material/menu';
import {FormControl, Validators, ReactiveFormsModule, FormsModule} from '@angular/forms';
import {FormGroupDirective, NgForm} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { GsevaService } from '../gseva.service';
import { LoadingModule } from 'ngx-loading';

@Component({
  selector: 'app-general-menu',
  templateUrl: './general-menu.component.html',
  styleUrls: ['./general-menu.component.css']
})
export class GeneralMenuComponent implements OnInit {

  constructor( private http:Http, private router:Router) { }
  agent:boolean=false;
  admin:boolean=false;
  ngOnInit() {
  	if (localStorage.getItem('userType')=='agent') {
  		this.agent=true;
  	}else if (localStorage.getItem('userType')=='admin') {
  		this.admin=true;
  	}
  }

  logout(){
  	localStorage.clear();
  	this.router.navigate(['login']);
  }

}
