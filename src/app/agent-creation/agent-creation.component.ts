import { Component, OnInit } from '@angular/core';
import {HttpModule, Http, URLSearchParams, Headers, RequestOptions} from '@angular/http';
import{ Router, ActivatedRoute, Params } from '@angular/router';
import {MatMenuModule} from '@angular/material/menu';
import {FormControl, Validators, ReactiveFormsModule, FormsModule} from '@angular/forms';
import {FormGroupDirective, NgForm} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { GsevaService } from '../gseva.service';
import { LoadingModule } from 'ngx-loading';




@Component({
  selector: 'app-agent-creation',
  templateUrl: './agent-creation.component.html',
  styleUrls: ['./agent-creation.component.css'],
  providers:[GsevaService]
})
export class AgentCreationComponent implements OnInit {

   constructor(private ref:GsevaService, private http:Http, private router:Router) { }
  public loading:boolean = false;
  apiRoot: string = this.ref.baseURL();
  msg:string;
  error:boolean=false;
  ngOnInit() {
  }


  createAgent(value){
    console.log(value);
    var requestData=[];
    requestData.push(value);
    let headers: Headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let opts = new RequestOptions();
    opts.headers = headers;
    this.http.post('http://localhost/gseva_api/users/create_agent.php',requestData,opts)
                  .map(response => response.json())
                  .subscribe((result) => {
                    console.log(result);
                    if(result.status=="failure"){
                      this.error=true;
                      this.msg=result.message;
                      this.loading = false;
                      alert(result.message);
                    }else if(result.status=="success"){
                      alert(result.message);
                    }
                  });
  }
      

}


